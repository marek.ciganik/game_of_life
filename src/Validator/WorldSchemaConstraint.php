<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

class WorldSchemaConstraint extends Constraint
{

    public string $message = 'Wrong XML format';
}