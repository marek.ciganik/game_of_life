<?php

namespace App\ValueObject;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

class XMLUpload
{

    #[Assert\File(
        maxSize: '1024k',
        mimeTypes: ['text/xml', 'application/xml'],
        mimeTypesMessage: 'Please upload a valid XML',
    )]
    private File $world;

    /**
     * @return File
     */
    public function getWorld(): File
    {

        return $this->world;
    }

    /**
     * @param File $world
     */
    public function setWorld(File $world): void
    {

        $this->world = $world;
    }
}