<?php

namespace App\Tests\WorldSimulation;

use App\Tests\PrivateMethodTestCase;
use App\ValueObject\Matrix;
use App\ValueObject\WorldInfo;
use App\WorldSimulation\ReplicationRuleResolver;
use App\WorldSimulation\WorldSimulator;
use ReflectionException;

class WorldSimulatorTest extends PrivateMethodTestCase
{

    /**
     * @throws ReflectionException
     * @dataProvider stepDataProvider
     */
    public function testStep(array $arrayMatrix, array $species, array $resolveReturn, array $resultExpectation)
    {

        $matrix = $this->createMatrix($arrayMatrix, $species);
        $replicationRuleResolver = $this->createMock(ReplicationRuleResolver::class);

        $replicationRuleResolver
            ->expects(self::any())
            ->method('resolve')
            ->will(self::returnValue($resolveReturn));

        $worldSimulator = new WorldSimulator($replicationRuleResolver);

        $result = self::callPrivateMethod($worldSimulator, 'step', [$matrix]);

        self::assertEquals($resultExpectation, $result);
    }

    /**
     * @throws ReflectionException
     * @dataProvider stepDataCompetitionProvider
     */
    public function testStepCompetition(
        array $arrayMatrix,
        array $species,
        array $resolveReturn,
        array $resultExpectation
    ) {

        $matrix = $this->createMatrix($arrayMatrix, $species);
        $replicationRuleResolver = $this->createMock(ReplicationRuleResolver::class);

        $replicationRuleResolver
            ->expects(self::any())
            ->method('resolve')
            ->will(self::returnValue($resolveReturn));

        $worldSimulator = new WorldSimulator($replicationRuleResolver);

        $result = self::callPrivateMethod($worldSimulator, 'step', [$matrix]);

        foreach ($result as $resultRow) {
            foreach ($resultRow as $resultCell) {
                self::assertContains($resultCell, $resultExpectation);
            }
        }
    }

    /**
     * @throws ReflectionException
     * @dataProvider getPossibleChangeCellsDataProvider
     */
    public function testGetPossibleChangeCells(array $arrayMatrix, array $species, array $resultExpectation)
    {

        $matrix = $this->createMatrix($arrayMatrix, $species);
        $replicationRuleResolver = $this->createMock(ReplicationRuleResolver::class);
        $worldSimulator = new WorldSimulator($replicationRuleResolver);

        $result = self::callPrivateMethod($worldSimulator, 'getPossibleChangeCells', [$matrix]);

        self::assertCount(count($resultExpectation), $result);
        foreach ($result as $resultRow) {
                self::assertContains($resultRow, $resultExpectation);
        }
    }

    private function stepDataProvider(): array
    {

        return [
            [
                [['', '', ''], ['', '', ''], ['', '', '']],
                ['spec1'],
                ['spec1'],
                [['', '', ''], ['', '', ''], ['', '', '']],
            ],
            [
                [['spec1', '', ''], ['', '', ''], ['', '', '']],
                ['spec1'],
                ['spec1'],
                [['spec1', 'spec1', ''], ['spec1', 'spec1', ''], ['', '', '']],
            ],
            [
                [['', '', ''], ['', '', ''], ['', '', '']],
                ['spec1'],
                [''],
                [['', '', ''], ['', '', ''], ['', '', '']],
            ],
        ];
    }

    private function stepDataCompetitionProvider(): array
    {

        return [
            [
                [['', '', ''], ['', 'spec1', ''], ['', '', '']],
                ['spec1', 'spec2'],
                ['spec1', 'spec2'],
                ['spec1', 'spec2'],
            ],
            [
                [['', '', ''], ['', 'spec1', ''], ['', '', '']],
                ['spec1', 'spec2', 'spec3', 'spec4', 'spec5', 'spec6'],
                ['spec1', 'spec2'],
                ['spec1', 'spec2'],
            ],
        ];
    }

    private function createMatrix(array $arrayMatrix, array $species): Matrix
    {

        $worldInfo = new WorldInfo(count($arrayMatrix), count($species), 1);

        return new Matrix($arrayMatrix, $worldInfo, $species);
    }

    private function getPossibleChangeCellsDataProvider(): array
    {

        return [
            // center
            [
                [['', '', ''], ['', 'spec1', ''], ['', '', '']],
                ['spec1'],
                [
                    ['x' => 0, 'y' => 0],
                    ['x' => 0, 'y' => 1],
                    ['x' => 0, 'y' => 2],
                    ['x' => 1, 'y' => 0],
                    ['x' => 1, 'y' => 1],
                    ['x' => 1, 'y' => 2],
                    ['x' => 2, 'y' => 0],
                    ['x' => 2, 'y' => 1],
                    ['x' => 2, 'y' => 2],
                ],
            ],
            // multiple, test union
            [
                [['', 'spec3', ''], ['spec1', 'spec1', 'spec2'], ['', '', '']],
                ['spec1', 'spec2', 'spec3'],
                [
                    ['x' => 0, 'y' => 0],
                    ['x' => 0, 'y' => 1],
                    ['x' => 0, 'y' => 2],
                    ['x' => 1, 'y' => 0],
                    ['x' => 1, 'y' => 1],
                    ['x' => 1, 'y' => 2],
                    ['x' => 2, 'y' => 0],
                    ['x' => 2, 'y' => 1],
                    ['x' => 2, 'y' => 2],
                ],
            ],
            // side
            [
                [['', 'spec3', ''], ['', '', ''], ['', '', '']],
                ['spec3'],
                [
                    ['x' => 0, 'y' => 0],
                    ['x' => 0, 'y' => 1],
                    ['x' => 1, 'y' => 0],
                    ['x' => 1, 'y' => 1],
                    ['x' => 2, 'y' => 0],
                    ['x' => 2, 'y' => 1],
                ],
            ],
            // corner
            [
                [['', '', ''], ['', '', ''], ['', '', 'spec3']],
                ['spec3'],
                [
                    ['x' => 1, 'y' => 1],
                    ['x' => 1, 'y' => 2],
                    ['x' => 2, 'y' => 1],
                    ['x' => 2, 'y' => 2],
                ],
            ],
        ];
    }
}
