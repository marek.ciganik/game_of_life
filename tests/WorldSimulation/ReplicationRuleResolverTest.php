<?php

namespace App\Tests\WorldSimulation;

use App\Tests\PrivateMethodTestCase;
use App\ValueObject\Matrix;
use App\ValueObject\WorldInfo;
use App\WorldSimulation\ReplicationRuleResolver;
use ReflectionException;

class ReplicationRuleResolverTest extends PrivateMethodTestCase
{

    /**
     * @dataProvider resolveDataProvider
     */
    public function testResolve(array $arrayMatrix, array $species, int $x, int $y, array $resultExpectation)
    {

        // setup
        $matrix = $this->createMatrix($arrayMatrix, $species);
        $replicationRuleResolver = new ReplicationRuleResolver();

        // execute
        $result = $replicationRuleResolver->resolve($matrix, $x, $y);

        // assert
        self::assertEquals($result, $resultExpectation);
    }
    /**
     *
     * @return array
     */
    private function resolveDataProvider(): array
    {

        return [
            [[['', '', ''], ['', '', ''], ['', '', '']], ['spec1'], 0, 0, []],
            [[['', '', ''], ['', 'spec1', ''], ['', '', '']], ['spec1'], 0, 0, []],
            [[['', '', ''], ['spec1', 'spec1', ''], ['', '', '']], ['spec1'], 0, 0, []],
            [[['', 'spec2', ''], ['spec1', 'spec1', ''], ['', '', '']], ['spec1', 'spec2'], 0, 0, []],
            // reproduce
            [[['', 'spec1', ''], ['spec1', 'spec1', ''], ['', '', '']], ['spec1'], 0, 0, ['spec1']],
            // survive
            [[['spec1', 'spec1', ''], ['spec1', '', ''], ['', '', '']], ['spec1'], 0, 0, ['spec1']],
            // competition
            [
                [['spec1', 'spec1', ''], ['spec1', '', 'spec2'], ['spec2', 'spec2', '']],
                ['spec1', 'spec2'],
                1,
                1,
                ['spec1', 'spec2'],
            ],
            // competition, one stand by
            [
                [['spec1', 'spec1', 'spec3'], ['spec1', '', 'spec2'], ['spec2', 'spec2', 'spec3']],
                ['spec1', 'spec2', 'spec3'],
                1,
                1,
                ['spec1', 'spec2'],
            ],
        ];
    }

    private function createMatrix(array $arrayMatrix, array $species): Matrix
    {

        $worldInfo = new WorldInfo(count($arrayMatrix), count($species), 1);
        return new Matrix($arrayMatrix, $worldInfo, $species);
    }
}
