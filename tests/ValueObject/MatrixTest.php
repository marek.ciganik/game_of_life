<?php

namespace App\Tests\ValueObject;

use App\ValueObject\Matrix;
use App\ValueObject\WorldInfo;
use PHPUnit\Framework\TestCase;

class MatrixTest extends TestCase
{
    /**
     * @dataProvider generateNeighborIndicesDataProvider
     */
    public function testNeighborhoodIndices(
        array $arrayMatrix,
        array $species,
        int $x,
        int $y,
        array $resultExpectation
    ) {

        // setup
        $matrix = $this->createMatrix($arrayMatrix, $species);

        // execute
        $result = $matrix->generateNeighborhoodIndices($x, $y);

        // assert
        self::assertCount(count($resultExpectation), $result);
        foreach ($result as $resultRow) {
            self::assertContains($resultRow, $resultExpectation);
        }
    }
    /**
     * @dataProvider neighborhoodDataProvider
     */
    public function testNeighborhood(array $arrayMatrix, array $species, int $x, int $y, array $resultExpectation)
    {

        // setup
        $matrix = $this->createMatrix($arrayMatrix, $species);

        // execute
        $result = $matrix->getNeighborhood($x, $y);

        // assert
        self::assertEquals($result, $resultExpectation);
    }
    /**
     *
     * @return array
     */
    private function generateNeighborIndicesDataProvider(): array
    {

        return [
            // corner, 3 indices
            [
                [['', '', ''], ['', '', ''], ['', '', '']],
                ['spec1'],
                0,
                0,
                [['x' => 1, 'y' => 0], ['x' => 1, 'y' => 1], ['x' => 0, 'y' => 1]],
            ],
            // whole neighborhood, 8 indices
            [
                [['', '', ''], ['', '', ''], ['', '', '']],
                ['spec1'],
                1,
                1,
                [
                    ['x' => 0, 'y' => 0],
                    ['x' => 0, 'y' => 1],
                    ['x' => 0, 'y' => 2],
                    ['x' => 1, 'y' => 0],
                    ['x' => 1, 'y' => 2],
                    ['x' => 2, 'y' => 0],
                    ['x' => 2, 'y' => 1],
                    ['x' => 2, 'y' => 2],
                ],
            ],
            // side, 5 indices
            [
                [['', '', ''], ['', '', ''], ['', '', '']],
                ['spec1'],
                2,
                1,
                [
                    ['x' => 1, 'y' => 0],
                    ['x' => 1, 'y' => 1],
                    ['x' => 1, 'y' => 2],
                    ['x' => 2, 'y' => 0],
                    ['x' => 2, 'y' => 2],
                ],
            ],
        ];
    }
    /**
     *
     * @return array
     */
    private function neighborhoodDataProvider(): array
    {

        return [
            [[['', '', ''], ['', 'spec1', ''], ['', '', '']], ['spec1'], 0, 0, ['spec1' => 1]],
            [
                [['', '', ''], ['spec2', 'spec1', ''], ['', '', '']],
                ['spec1', 'spec2'],
                0,
                0,
                ['spec1' => 1, 'spec2' => 1],
            ],
            [
                [['spec2', 'spec2', ''], ['spec2', 'spec1', ''], ['', '', '']],
                ['spec1', 'spec2'],
                0,
                0,
                ['spec1' => 1, 'spec2' => 2],
            ],
            [
                [['spec1', 'spec1', 'spec1'], ['spec1', 'spec1', 'spec1'], ['spec1', 'spec1', 'spec1']],
                ['spec1'],
                1,
                1,
                ['spec1' => 8],
            ],
            [[['', '', ''], ['', 'spec1', ''], ['', '', '']], ['spec1'], 1, 1, ['spec1' => 0]],
            [[['', '', ''], ['', 'spec1', ''], ['', '', '']], ['spec1', 'spec2'], 1, 1, ['spec1' => 0, 'spec2' => 0]],
        ];
    }

    private function createMatrix(array $arrayMatrix, array $species): Matrix
    {

        $worldInfo = new WorldInfo(count($arrayMatrix), count($species), 1);
        return new Matrix($arrayMatrix, $worldInfo, $species);
    }
}
